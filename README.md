# 致知

“致知”微信小程序，实现幼儿学习语文拼音、古诗、英文字母、英文单词、数学加减乘除。

## 工作内容

- 使用 NABCD 构建需求模型，墨刀制作系统原型
- 微信小程序实现项目，使用 JavaScript 和 CSS 开发前端界面
- 使用微信小程序的云函数和云数据库实现数据处理和存储
