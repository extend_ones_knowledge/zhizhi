// pages/song/song.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //古诗目录类
    lists: []
  },
  onLoad: function (options) {
    

    var page = this;
    var lists = this.loadPoetrysListData();
    page.setData({ lists: lists });
  },
  listBtn: function (e) {
    console.log(e);
    var id = e.currentTarget.id;
    
  },
  loadPoetrysListData: function () {
    var lists = [];
    var list0 = new Object();
    list0.id = '0';
    list0.name = '蓝皮鼠和大脸猫';
    list0.src = 'http://ting.xiai123.com/mp3/youxiuerge-1/02.蓝皮鼠和大脸猫 - 群星.mp3';
    list.author = '经典儿歌';
    list0.poster = 'http://ting.xiai123.com/images/youxiuerge-1.jpg';
    list0.add_date = '2019-10-10';
    lists[0] = list0;

    var list1 = new Object();
    list1.id = '1';
    list1.name = '鲁冰花';
    list1.src = 'http://ting.xiai123.com/mp3/youxiuerge-1/20.鲁冰花 - 群星.mp3';
    list1.author = '经典儿歌';
    list1.poster = 'http://ting.xiai123.com/images/youxiuerge-1.jpg';
    list1.add_date = '2019-10-10';
    lists[1] = list1;

    var list2 = new Object();
    list2.id = '2';
    list2.name = '虫儿飞';
    list2.src = 'http://ting.xiai123.com/mp3/youxiuerge-1/虫儿飞.mp3';
    list2.author = '经典儿歌';
    list2.poster = 'http://ting.xiai123.com/images/youxiuerge-1.jpg';
    list2.add_date = '2019-10-10';
    lists[2] = list2;

    return lists;
  },
  audioPlay: function (options) {
    this.audioCtx.play()
  },
  audioPause: function (options) {
    this.audioCtx.pause()
  },
  audioSeek0: function (options) {
    this.audioCtx.seek(0)
  },

  onReady: function () {
    this.audioCtx = wx.createAudioContext('myAudio')
    //this.audioCtx.play()
  }
})