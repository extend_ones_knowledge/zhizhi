// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    poetry: {
      id: '0',
      title: '古诗词三首 宿建德江',
      poster: 'http://url.yunser.net/v11FA',
      name: '宿建德江',
      author: '孟浩然',
      content1: '移舟泊烟渚，日暮客愁新。',
      content2: '野旷天低树，江清月近人。',
      src: 'http://ting.xiai123.com/mp3/kewen/2019qiu-rjyw6s/3 古诗词三首 宿建德江.mp3',
      add_date: '2019-10-10'
    }
  },

  audioPlay: function (options) {
    this.audioCtx.play()
  },
  audioPause: function (options) {
    this.audioCtx.pause()
  },
  audioSeek0: function (options) {
    this.audioCtx.seek(0)
  },

  onReady: function () {
    this.audioCtx = wx.createAudioContext('myAudio')
    //this.audioCtx.play()
  },
})