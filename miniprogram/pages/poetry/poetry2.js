// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    poetry: {
      id: '2',
      title: '古诗词三首 西江月·夜行黄沙道中',
      poster: 'http://url.yunser.net/v11FE',
      add_date: '2019-10-10',
      name: '西江月·夜行黄沙道中',
      author: '辛弃疾',
      content1: '明月别枝惊鹊，清风半夜鸣蝉。',
      content2:' 稻花香里说丰年，听取蛙声一片。',
      content3:' 七八个星天外，两三点雨山前。',
      content4:' 旧时茅店社林边，路转溪桥忽见。',
      src: 'http://ting.xiai123.com/mp3/kewen/2019qiu-rjyw6s/3 古诗词三首 西江月·夜行黄沙道中.mp3'
    }
  },

  audioPlay: function (options) {
    this.audioCtx.play()
  },
  audioPause: function (options) {
    this.audioCtx.pause()
  },
  audioSeek0: function (options) {
    this.audioCtx.seek(0)
  },

  onReady: function () {
    this.audioCtx = wx.createAudioContext('myAudio')
    //this.audioCtx.play()
  },
})