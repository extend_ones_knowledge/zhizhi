
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //幻灯片素材
    swiperImg: [
      { src: 'http://url.yunser.net/v11Fz' },
      { src: 'http://url.yunser.net/v11FA' },
      { src: 'http://url.yunser.net/v11FE' }
    ],
    
    //古诗目录类
    lists: []
  },

  onLoad: function (options) {
    var page = this;
    var lists = this.loadPoetrysListData();
    page.setData({ lists: lists });
  },
  listBtn: function (e) {
    console.log(e);
    var id = e.currentTarget.id;
    if (id == "0") {
      wx.navigateTo({
        url: '../poetry/poetry'
      })
    }
    if (id == "1") {
      wx.navigateTo({
        url: '../poetry/poetry1'
      })
    }
    if (id == "2") {
      wx.navigateTo({
        url: '../poetry/poetry2'
      })
    }
  },
  loadPoetrysListData: function () {
    var lists = [];
    var list0 = new Object();
    list0.id = '0';
    list0.title='古诗词三首 宿建德江';
    list0.poster='http://url.yunser.net/v11FA';
    list0.add_date= '2019-10-10';
    lists[0]=list0;

    var list1 = new Object();
    list1.id = '1';
    list1.title = '古诗词三首 六月二十七日望湖楼醉书';
    list1.poster = 'http://url.yunser.net/v11Fz';
    list1.add_date = '2019-10-10';
    lists[1] = list1;
   
    var list2 = new Object();
    list2.id = '2';
    list2.title = '古诗词三首 西江月·夜行黄沙道中';
    list2.poster = 'http://url.yunser.net/v11FE';
    list2.add_date = '2019-10-10';
    lists[2] = list2;

    return lists;
  }

})