// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    poetry: {
      id: '1',
      title: '古诗词三首 六月二十七日望湖楼醉书',
      poster: 'http://url.yunser.net/v11Fz',
      name: '六月二十七日望湖楼醉书',
      author: '苏轼',
      src: 'http://ting.xiai123.com/mp3/kewen/2019qiu-rjyw6s/3 古诗词三首 六月二十七日望湖楼醉书.mp3',
      add_date: '2019-10-10',
      content1:'黑云翻墨未遮山，白雨跳珠乱入船。',
      content2:' 卷地风来忽吹散，望湖楼下水如天。'
    }
  },

  audioPlay: function (options) {
    this.audioCtx.play()
  },
  audioPause: function (options) {
    this.audioCtx.pause()
  },
  audioSeek0: function (options) {
    this.audioCtx.seek(0)
  },

  onReady: function () {
    this.audioCtx = wx.createAudioContext('myAudio')
    //this.audioCtx.play()
  },
})