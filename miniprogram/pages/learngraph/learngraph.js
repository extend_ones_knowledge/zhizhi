// pages/learngraph/learngraph.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [{
      mode: 'scaleToFill',
      text: '平移',
      src: '../../images/tuxing/pingyi.gif'
    },{
        mode: 'scaleToFill',
        text: '旋转',
        src: 'http://img.xiazaizhijia.com/uploads/2017/0809/20170809044559266.gif'
    },{
        mode: 'scaleToFill',
        text: '轴对称',
        src: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571233998202&di=4a0b2c7dbe0c86073da191c1176887ab&imgtype=0&src=http%3A%2F%2Fs13.sinaimg.cn%2Fbmiddle%2F5fa51223g67c05f27035c'
    }, {
        mode: 'scaleToFill',
        text: '中心对称',
        src: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571234158874&di=b78f6b8e95ee30e2a133795bd4e8d2bc&imgtype=0&src=http%3A%2F%2Fimage.tigu.cn%2Fpic%2Fspic%2F5%2F5026%2F50261175-3.png'
    }]
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})