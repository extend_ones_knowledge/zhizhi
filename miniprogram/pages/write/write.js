// pages/write/write.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navs: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var page = this;
    var navs = this.loadNavData();
    page.setData({ navs: navs });
  },
  loadNavData: function () {
    var navs = [];
    var nav0 = new Object();
    nav0.img = 'http://bishun.strokeorder.info/characters/927148.gif';
    nav0.name = '百的部首：白  百的笔画数：6';
    navs[0] = nav0;

    var nav1 = new Object();
    nav1.img = 'http://bishun.strokeorder.info/characters/115000.gif';
    nav1.name = '长的部首：长     长的笔画数：4';
    navs[1] = nav1;

    var nav2 = new Object();
    nav2.img = 'http://bishun.strokeorder.info/characters/502730.gif';
    nav2.name = '乘的部首：丿     乘的笔画数：10';
    navs[2] = nav2;

    var nav3 = new Object();
    nav3.img = 'http://bishun.strokeorder.info/characters/738179.gif';
    nav3.name = '除的部首：阝     除的笔画数：9';
    navs[3] = nav3;

    var nav4 = new Object();
    nav4.img = 'http://bishun.strokeorder.info/characters/298710.gif';
    nav4.name = '春的部首：日     春的笔画数：9';
    navs[4] = nav4;

    var nav5 = new Object();
    nav5.img = 'http://bishun.strokeorder.info/characters/704829.gif';
    nav5.name = '大的部首：大     大的笔画数：3';
    navs[5] = nav5;

    var nav6 = new Object();
    nav6.img = 'http://bishun.strokeorder.info/characters/859121.gif';
    nav6.name = '冬的部首：夂     冬的笔画数：5';
    navs[6] = nav6;

    var nav7 = new Object();
    nav7.img = 'http://bishun.strokeorder.info/characters/147535.gif';
    nav7.name = '短的部首：矢     短的笔画数：12';
    navs[7] = nav7;

    var nav8 = new Object();
    nav8.img = 'http://bishun.strokeorder.info/characters/148049.gif';
    nav8.name = '多的部首：夕     多的笔画数：6';
    navs[8] = nav8;

    var nav9 = new Object();
    nav9.img = 'http://bishun.strokeorder.info/characters/863276.gif';
    nav9.name = '个的部首：人     个的笔画数：3';
    navs[9] = nav9;

    var nav10 = new Object();
    nav10.img = 'http://bishun.strokeorder.info/characters/886634.gif';
    nav10.name = '后的部首：口     后的笔画数：6';
    navs[10] = nav10;

    var nav11 = new Object();
    nav11.img = 'http://bishun.strokeorder.info/characters/825126.gif';
    nav11.name = '加的部首：力     加的笔画数：5';
    navs[11] = nav11;

    var nav12 = new Object();
    nav12.img = 'http://bishun.strokeorder.info/characters/175296.gif';
    nav12.name = '减的部首：冫     减的笔画数：11';
    navs[12] = nav12;

    var nav13 = new Object();
    nav13.img = 'http://bishun.strokeorder.info/characters/824784.gif';
    nav13.name = '前的部首：刂     前的笔画数：9';
    navs[13] = nav13;

    var nav14 = new Object();
    nav14.img = 'http://bishun.strokeorder.info/characters/240521.gif';
    nav14.name = '秋的部首：禾     秋的笔画数：9';
    navs[14] = nav14;

    var nav15 = new Object();
    nav15.img = 'http://bishun.strokeorder.info/characters/653775.gif';
    nav15.name = '少的部首：小     少的笔画数：4';
    navs[15] = nav15;

    var nav16 = new Object();
    nav16.img = 'http://bishun.strokeorder.info/characters/837654.gif';
    nav16.name = '十的部首：十     十的笔画数：2';
    navs[16] = nav16;

    var nav17 = new Object();
    nav17.img = 'http://bishun.strokeorder.info/characters/60240.gif';
    nav17.name = '千的部首：十     千的笔画数：3';
    navs[17] = nav17;

    var nav18 = new Object();
    nav18.img = 'http://bishun.strokeorder.info/characters/275987.gif';
    nav18.name = '万的部首：一     万的笔画数：3';
    navs[18] = nav18;

    var nav19 = new Object();
    nav19.img = 'http://bishun.strokeorder.info/characters/363970.gif';
    nav19.name = '小的部首：小     小的笔画数：3';
    navs[19] = nav19;

    var nav20 = new Object();
    nav20.img = 'http://bishun.strokeorder.info/characters/217875.gif';
    nav20.name = '夏的部首：夂     夏的笔画数：10';
    navs[20] = nav20;

    var nav21 = new Object();
    nav21.img = 'http://bishun.strokeorder.info/characters/810136.gif';
    nav21.name = '亿的部首：亻     亿的笔画数：3';
    navs[21] = nav21;

    var nav22 = new Object();
    nav22.img = 'http://bishun.strokeorder.info/characters/91318.gif';
    nav22.name = '右的部首：口     右的笔画数：5';
    navs[22] = nav22;

    var nav23 = new Object();
    nav23.img = 'http://bishun.strokeorder.info/characters/263790.gif';
    nav23.name = '左的部首：工     左的笔画数：5';
    navs[23] = nav23;
    return navs;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})