// pages/type/type.js
Page({
  nav1:function(){
    wx.navigateTo({
      url: '../pinyintable/pinyin',
    })
  },
  nav6: function () {
    wx.navigateTo({
      url: '../lettertable/letter',
    })
  },
  nav10: function () {
    wx.navigateTo({
      url: '../mutitable/muti',
    })
  },
  nav11: function () {
    wx.navigateTo({
      url: '../plustable/plustable',
    })
  },
  nav3: function () {
    wx.navigateTo({
      url: '../poetry/poetrycontent',
    })
  },
  nav13: function () {
    wx.navigateTo({
      url: '../learngraph/learngraph',
    })
  },
  nav12: function () {
    wx.navigateTo({
      url: '../unitchange/unitchange',
    })
  },
  nav5: function () {
    wx.navigateTo({
      url: '../write/write',
    })
  },
  nav8: function () {
    wx.navigateTo({
      url: '../research/researchletter',
    })
  },
  nav15: function () {
    wx.navigateTo({
      url: '../graphcount/graphcount',
    })
  },
  nav7: function () {
    wx.navigateTo({
      url: '../learnletter/learnletter',
    })
  },
  nav14: function () {
    wx.navigateTo({
      url: '../calculate/calculate',
    })
  },
})