// pages/research/researchletter.js
var app = getApp()
const db = wx.cloud.database()
var util = require("../../utils/util.js")

Page({
  /**
   * 页面的初始数据
   */
  data: {
    items: [
      { name: '1', value: '答案不能为负' },
      { name: '2', value: '答案不能有小数', },
    ],
    navs:[],//题目数组
    unavs:[],//用户答案
    uflag:[],//正确标志
    navs1:"",//当前显示的题目的内容

    show_ans:[],
    
    inputText:''
    
  },
  numbers: 0,//初始化
  maxi: 0,
  
  num: function (e) {//获取题目数
    if(e.detail.value < 0){
      
      wx.showModal({
        title: '提示',

        content: '请输入大于0的数字',
        showCancel: false
      })

      return;
    }
    this.numbers = e.detail.value
    console.log("出题数为" + e.detail.value)
  },
  max: function (e) {//获取最大数限制
    this.maxi = e.detail.value
    console.log("运算数最大为" + e.detail.value)
  },
  formSubmit: function (e) {
    this.data.items[0].checked = true
    {
      wx.showModal({
        title: '',
        content: '111',
      })
    } console.log('form发生了submit事件，携带数据为：', e.detail.value)
  },
  p1:0,
  p2:0,
  fuhao1:'',
  fuhao2:'',
  
  
  mains:function(){//随机生成运算式
      var count=this.numbers;//题目的数量
      //var count = 10;
      var a,b,c,result;
      var navs = [];
      while(count>0){
        
        this.p1 = Math.floor(Math.random() * 4 + 1);
        this.p2 = Math.floor(Math.random() * 4 + 1);
        switch (this.p1) {
          case 1: this.fuhao1 = '+'; break;
          case 2: this.fuhao1 = '-'; break;
          case 3: this.fuhao1 = '×'; break;
          case 4: this.fuhao1 = '÷'; break;
        }

        switch (this.p2) {
          case 1: this.fuhao2 = '+'; break;
          case 2: this.fuhao2 = '-'; break;
          case 3: this.fuhao2 = '×'; break;
          case 4: this.fuhao2 = '÷'; break;
        }

        a = Math.floor(Math.random() * this.maxi + 0);
        b = Math.floor(Math.random() * this.maxi + 0);
        c = Math.floor(Math.random() * this.maxi + 0);
        switch(this.p1){
          case 1:
               switch(this.p2){
                 case 1: result = a + b + c; break;
                 case 2: result = a + b - c; break;
                 case 3: result = a + b * c; break;
                 case 4: result = a + b / c; break;
               }
               break;
          case 2:
            switch (this.p2) {
              case 1: result = a - b + c; break;
              case 2: result = a - b - c; break;
              case 3: result = a - b * c; break;
              case 4: result = a - b / c; break;
            }
             break;
          case 3:
            switch (this.p2) {
              case 1: result = a * b + c; break;
              case 2: result = a * b - c; break;
              case 3: result = a * b * c; break;
              case 4: result = a * b / c; break;
            }
            break;
          case 4:
            switch (this.p2) {
              case 1: result = a / b + c; break;
              case 2: result = a / b - c; break;
              case 3: result = a / b * c; break;
              case 4: result = a / b / c; break;
            }
            break;
        }
        if(result%1===0 && result > 0 && result <this.maxi){
          count--;
          var nav = new Object();
          //this.text.push(a+this.fuhao1+b+this.fuhao2+c+"="+result);
          nav.text = a + this.fuhao1 + b + this.fuhao2 + c + "=";
          nav.answer  = result;
          
          navs.push(nav);
        }
      }
      return navs;
  },
  text:[],
  /**
   * 生命周期函数--监听页面加载
   */
  getTotal:function(){
      return 
  },
  firstid:0,
  nowid:0,
  lastid:0,
  //uid:'',
  makes:function() {//初始化题库
    var page = this;
    var navs = this.mains();
    page.setData({
      navs:navs
    })
    //var aaid;
    //var uid;
    var that = this;
    this.lastid = this.firstid + this.numbers
    this.nowid = this.firstid

    var show=[]
    for(var j=0;j<this.numbers;j++){
      show[j]=''
    }
    page.setData({
      show_ans:show
    })

    page.setData({
      navs1:navs[this.nowid].text
    })
  },
  uans:'',//用户输入的答案
  ans0:function(e){//获取用户答案
    if (!(/^[0-9]+$/.test(e.detail.value))) {
      wx.showModal({
        title: '提示',

        content: '请输入数字',
        showCancel: false
      })

      return;
    }
    this.uans = e.detail.value;
    var that = this
    var i = e.detail.value;
    this.data.unavs[this.nowid] = i//存储用户答案的数组

    if(i == this.data.navs[this.nowid].answer){
      this.data.uflag[this.nowid] = 1
    }
    else this.data.uflag[this.nowid] = 0
  },
  answer:function(){//查看答案按钮
    /*
    if (!(/^[0-9]+$/.test(this.uans))){
      wx.showModal({
        title: '提示',

        content: '请输入数字',
        showCancel: false
      })

      return;
    }
    */
    var navs = this.data.navs//题目答案
    var unavs = this.data.unavs//用户答案
    var flag = this.data.uflag//正确标志
    var show = this.data.show_ans
    var strings

    for (var i = 0; i < this.numbers; i++) {//取出所有题目
      strings = '题目：'+navs[i].text+'你的答案：'+unavs[i]+'正确答案：'+show[i]
      wx.showModal({
        title: '提示',

        content: strings,
        showCancel: false
      })
    }
  },
  setquiz:function(){
      
  },
  next:function(){//下一道题
     this.nowid  = this.nowid +1
     var that = this

    if(this.nowid >= this.lastid){
      this.nowid = this.nowid - 1
      wx.showModal({
        title: '提示',

        content: '已经是最后一题，请确认无误提交',
        showCancel: false
      })
      return
    }
    var input = this.data.unavs[this.nowid]
     that.setData({
       inputText:input
     })
    that.setData({
      navs1: ''
    })
    var text = this.data.navs[this.nowid].text
     that.setData({
       navs1: text
     })
  },
  back: function () {//上一道题
    this.nowid = this.nowid - 1
    var that = this

    if (this.nowid < this.firstid) {
      this.nowid = this.nowid + 1
      wx.showModal({
        title: '提示',

        content: '已经是第一题，不能往前了',
        showCancel: false
      })
      return
    }
    var input = this.data.unavs[this.nowid]
    that.setData({
      inputText: input
    })
    that.setData({
      navs1: ''
    })
    var text = this.data.navs[this.nowid].text
    that.setData({
      navs1: text
    })

  },
  submit: function () {//提交本地数据到云数据库
    var navs = this.data.navs
    var unavs = this.data.unavs
    var flag = this.data.uflag
    
    var show_ans=[]
    var that = this 
    for(var j=0;j<this.numbers;j++){
      show_ans[j] = navs[j].answer
    }
    that.setData({
      show_ans:show_ans
    })


    for (var i = 0; i < this.numbers; i++){//取出所有题目
       db.collection('A-ku').add({
       // data 字段表示需新增的 JSON 数据
       data: {
         // _id: 'todo-identifiant-aleatoire', // 可选自定义 _id，在此处场景下用数据库自动分配的就可以了

           ANS: unavs[i],
           CONT: navs[i].text,
           COR: flag[i],
           COR_ANS: navs[i].answer,
           //UID: uid,
           Date: new Date(),
        },
        success: function (res) {
          // res 是一个对象，其中有 _id 字段标记刚创建的记录的 id
           console.log(res.data)
        }
      })
      console.log('插入成功')
  }
  },
  error: function () {//跳转到错题页面
    wx.navigateTo({
      url: '../errquiz/errquiz'
    })
  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
//----------------------------time---------------------------------------
  getDates:function (days, todate) {
    var dateArry = [];
    module.exports = {
      formatDate: formatDate,
      getDates: getDates
    }
    for(var i = 0; i<days; i++) {
       var dateObj = dateLater(todate, i);
       dateArry.push(dateObj)
    }
    return dateArry;
  },
dateLater:function (dates, later) {
  module.exports = {
    formatDate: formatDate,
    getDates: getDates
  }
  let dateObj = {};
  let show_day = new Array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
  let date = new Date(dates);
  date.setDate(date.getDate() + later);
  let day = date.getDay();
  let yearDate = date.getFullYear();
  let month = ((date.getMonth() + 1) < 10 ? ("0" + (date.getMonth() + 1)) : date.getMonth() + 1);
  let dayFormate = (date.getDate() < 10 ? ("0" + date.getDate()) : date.getDate());
  dateObj.time = yearDate + '-' + month + '-' + dayFormate;
  dateObj.week = show_day[day];
  return dateObj;
}

})
/*
<view class="nav">
 <block wx:for="{{navs}}">
   <view class="item" bindtap="navBtn" id="{{index}}">
      <view>{{item.text}}</view>
      <view class="hr"></view>
   </view>
   </block>
 </view>
  <view class="hr"></view>
*/