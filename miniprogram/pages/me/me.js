var app = getApp()
Page({
  data: {
    userInfo: {}
  },
  onLoad: function (options) {
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function (userInfo) {
      console.log(userInfo);
      //更新数据
      that.setData({
        userInfo: userInfo
      })
    })
  },
  getMyInfo: function (e) {
    let info = e.detail.userInfo;
    this.setData({
      isLogin: true, //确认登陆状态
      src: info.avatarUrl,   //更新图片来源
      nickName: info.nickName //更新昵称
    })

  },
  grabTicket: function () {
    wx.navigateTo({
      url: '../grabticket/grabticket'
    })
  },
  msg : function () {
    wx.navigateTo({
      url: '../msg/msg'
    })
  }
})
//api was done