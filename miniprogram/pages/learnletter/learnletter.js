Page({

  /**
   * 页面的初始数据
   */
  data: {
    navs: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var page = this;
    var navs = this.loadNavData();
    page.setData({ navs: navs });
  },
  loadNavData: function () {
    var navs = [];
    var nav0 = new Object();
    nav0.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570832012422&di=e0768a064832bf276da3f5a8c08e2e51&imgtype=0&src=http%3A%2F%2Fdpic.tiankong.com%2Fz7%2F86%2FQJ6610322829.jpg%3Fx-oss-process%3Dstyle%2Fshow';
    nav0.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=apple&spd=2&source=alading';
    nav0.name = 'apple';
    nav0.definition = '苹果';
    navs[0] = nav0;

    var nav1 = new Object();
    nav1.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831975351&di=46f83ccb78098bda08fd62df0d83feaa&imgtype=0&src=http%3A%2F%2Fimg.sccnn.com%2Fbimg%2F313%2F102.jpg';
    nav1.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=bird&spd=2&source=alading';
    nav1.name = 'bird';
    nav1.definition = '小鸟';
    navs[1] = nav1;

    var nav2 = new Object();
    nav2.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831949159&di=2dce5733b81a282c4bc987a9d5481320&imgtype=0&src=http%3A%2F%2Fphotocdn.sohu.com%2F20120718%2FImg348433926.jpg';
    nav2.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=cat&spd=2&source=alading';
    nav2.name = 'cat';
    nav2.definition = '猫';
    navs[2] = nav2;

    var nav3 = new Object();
    nav3.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831909726&di=b6229df7c656675d3d7a4f5f86194f94&imgtype=0&src=http%3A%2F%2Fi1.hdslb.com%2Fbfs%2Fface%2F33cdb6a8e27df3441e5008e59a33301e881379b8.jpg';
    nav3.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=dog&spd=2&source=alading';
    nav3.name = 'dog';
    nav3.definition = '狗';
    navs[3] = nav3;

    var nav4 = new Object();
    nav4.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831859645&di=eb31c31b363e484ebc63c7bd58940a72&imgtype=0&src=http%3A%2F%2Fimg1.99114.com%2Fgroup10%2FM00%2F49%2F2B%2FrBADs1oKtJOAano8AABD58AbMFc893.jpg';
    nav4.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=egg&spd=2&source=alading';
    nav4.name = 'egg';
    nav4.definition = '鸡蛋';
    navs[4] = nav4;

    var nav5 = new Object();
    nav5.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831822383&di=913f97b676138f55a3d6e930d3ef1470&imgtype=0&src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F18%2F01%2F31%2F9101235384f1c92c83fadb8ef8cfd918.jpg%2521%2Ffwfh%2F804x804%2Fquality%2F90%2Funsharp%2Ftrue%2Fcompress%2Ftrue';
    nav5.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=frog&spd=2&source=alading';
    nav5.name = 'frog';
    nav5.definition = '青蛙';
    navs[5] = nav5;

    var nav6 = new Object();
    nav6.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571426475&di=98615b802f179bee5716c8db92308a4c&imgtype=jpg&er=1&src=http%3A%2F%2Fi.ebayimg.com%2F00%2Fs%2FMTIwMFgxMjAw%2Fz%2F7fYAAOSwWiBY%7ETx5%2F%24_57.JPG%3Fset_id%3D8800005007';
    nav6.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=guitar&spd=2&source=alading';
    nav6.name = 'guitar';
    nav6.definition = '吉他';
    navs[6] = nav6;

    var nav7 = new Object();
    nav7.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831720829&di=b8bae5787bae3dfd1fc19e2c19ce508e&imgtype=jpg&src=http%3A%2F%2Fpic.cnitblog.com%2Favatar%2Fa470820.png%3Fid%3D20221821';
    nav7.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=head&spd=2&source=alading';
    nav7.name = 'head';
    nav7.definition = '头';
    navs[7] = nav7;

    var nav8 = new Object();
    nav8.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571426365&di=91bb78fefc1ea4bb24312dd9ad5b5fcb&imgtype=jpg&er=1&src=http%3A%2F%2Fecx.images-amazon.com%2Fimages%2FI%2F31h0qfhOoxL.jpg';
    nav8.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=ink&spd=2&source=alading';
    nav8.name = 'ink';
    nav8.definition = '墨水';
    navs[8] = nav8;

    var nav9 = new Object();
    nav9.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831579437&di=38e98d3585ccefaf9296ef2d148ba0e3&imgtype=0&src=http%3A%2F%2Fimg.frbiz.com%2Fpic%2Fz1d44ad0-300x300-1%2Fjacket.jpg';
    nav9.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=jacket&spd=2&source=alading';
    nav9.name = 'jacket';
    nav9.definition = '夹克衫';
    navs[9] = nav9;

    var nav10 = new Object();
    nav10.img = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570831496336&di=d0cf379b64cf349dc6717a9b2d449823&imgtype=0&src=http%3A%2F%2Fpic2.orsoon.com%2F2017%2F0425%2F20170425015457423.png';
    nav10.audio = 'https://sp0.baidu.com/-rM1hT4a2gU2pMbgoY3K/gettts?lan=uk&text=kite&spd=2&source=alading';
    nav10.name = 'kite';
    nav10.definition = '风筝';
    navs[10] = nav10;

    return navs;
  },
  audioPlay: function (options) {
    this.audioCtx.play()
  },
  audioPause: function (options) {
    this.audioCtx.pause()
  },
  audioSeek0: function (options) {
    this.audioCtx.seek(0)
  },

  onReady: function () {
    this.audioCtx = wx.createAudioContext('myAudio')
    //this.audioCtx.play()
  },
  /*
    audioPlay: function (options) {
      this.audioCtx.play()
    },
    audioPause: function (options) {
      this.audioCtx.pause()
    },
  */
})