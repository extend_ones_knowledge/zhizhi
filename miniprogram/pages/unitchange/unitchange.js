// pages/unitchange/unitchange.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [{
      mode: 'scaleToFill',
      text: '长度单位换算'
    }, {
        mode: 'scaleToFill',
        text: '1千米=1000米 1米=10分米'
      }, {
        mode: 'scaleToFill',
        text: '1分米=10厘米 1米=100厘米'
      }, {
        mode: 'scaleToFill',
        text: '1厘米=10毫米'
      }, {
        mode: 'scaleToFill',
        text: '面积单位换算'
      }, {
        mode: 'scaleToFill',
        text: '1平方千米=100公顷'
      }, {
        mode: 'scaleToFill',
        text: '1公顷=10000平方米'
      }, {
        mode: 'scaleToFill',
        text: '1平方米=100平方分米'
      }, {
        mode: 'scaleToFill',
        text: '1平方分米=100平方厘米'
      }, {
        mode: 'scaleToFill',
        text: '1平方厘米=100平方毫米'
      }, {
        mode: 'scaleToFill',
        text: '体(容)积单位换算'
      }, {
        mode: 'scaleToFill',
        text: '1立方米=1000立方分米'
      }, {
        mode: 'scaleToFill',
        text: '1立方分米=1000立方厘米'
      }, {
        mode: 'scaleToFill',
        text: '1立方分米=1升'
      }, {
        mode: 'scaleToFill',
        text: '1立方厘米=1毫升'
      }, {
        mode: 'scaleToFill',
        text: '1立方米=1000升'
      }, {
        mode: 'scaleToFill',
        text: '重量单位换算'
      }, {
        mode: 'scaleToFill',
        text: '1吨=1000千克'
      }, {
        mode: 'scaleToFill',
        text: '1千克=1000克'
      }, {
        mode: 'scaleToFill',
        text: '1千克=1公斤'
      }, {
        mode: 'scaleToFill',
        text: '人民币单位换算'
      }, {
        mode: 'scaleToFill',
        text: '1元=10角'
      }, {
        mode: 'scaleToFill',
        text: '1角=10分'
      }, {
        mode: 'scaleToFill',
        text: '1元=100分'
      }, {
        mode: 'scaleToFill',
        text: '时间单位换算'
      }, {
        mode: 'scaleToFill',
        text: '1世纪=100年1年=12月'
      }, {
        mode: 'scaleToFill',
        text: '大月(31天)有:135781012月'
      }, {
        mode: 'scaleToFill',
        text: '小月(30天)的有:46911月'
      }, {
        mode: 'scaleToFill',
        text: '平年2月28天,闰年2月29天'
      }, {
        mode: 'scaleToFill',
        text: '平年全年365天,闰年全年366天'
      }, {
        mode: 'scaleToFill',
        text: '1日=24小时1时=60分'
      },{
        text:'1分=60秒1时=3600秒'
      }],
    src: '../../images/table/plus.jpg'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})