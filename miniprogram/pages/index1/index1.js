Page({
  data: {
    indicatorDots: false,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    imgUrls: [
      "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1573543457942&di=db1277551cb73103633a1933cd9e87b3&imgtype=jpg&src=http%3A%2F%2Fimg2.imgtn.bdimg.com%2Fit%2Fu%3D2618442542%2C3808788937%26fm%3D214%26gp%3D0.jpg",
      "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571235412414&di=c3790736e4068288066d7a7911ed9fc9&imgtype=jpg&src=http%3A%2F%2Fimg0.imgtn.bdimg.com%2Fit%2Fu%3D2581646935%2C4270026998%26fm%3D214%26gp%3D0.jpg",
      "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571235759217&di=fcf9d1d2b4fd8389b9dacbf565db8f54&imgtype=0&src=http%3A%2F%2Fimg.gmw.cn%2Fimages%2Fattachement%2Fpng%2Fsite2%2F20170208%2Ff44d305ea5951a053b8436.png",
      "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1573543566305&di=194318d8202007d6f6b82f0b3dfaa60b&imgtype=0&src=http%3A%2F%2Fimg02.sogoucdn.com%2Fapp%2Fa%2F200698%2F650_450_F45CCD9AE6AE624D81485FB8D736B88A"
    ],
    navs: []
  },
  onLoad: function (options) {
    const db = wx.cloud.database()
    
    db.collection('maxid').get({
      success(res) {
        //console.log(dbMAX+"为")
        console.log(res.data[0].MAX)
      }
    })

    var page = this;
    var navs = this.loadNavData();
    page.setData({ navs: navs });
  },
  navBtn: function (e) {
    console.log(e);
    var id = e.currentTarget.id;
    if (id == "9") {
      wx.navigateTo({
        url: '../type/type'
      })
    }
    if (id == "3") {
      wx.navigateTo({
        url: '../plustable/plustable'
      })
    }
    if (id == "2") {
      wx.navigateTo({
        url: '../mutitable/muti'
      })
    }
    if (id == "0") {
      wx.navigateTo({
        url: '../pinyintable/pinyin'
      })
    }
    if (id == "6") {
      wx.navigateTo({
        url: '../lettertable/letter'
      })
    }
    if (id == "1") {
      wx.navigateTo({
        url: '../poetry/poetrycontent'
      })
    }
    if (id == "4") {
      wx.navigateTo({
        url: '../unitchange/unitchange'
      })
    }
    if (id == "5") {
      wx.navigateTo({
        url: '../learngraph/learngraph'
      })
    }
    if (id == "7") {
      wx.navigateTo({
        url: '../research/researchletter'
      })
    }

  },
  loadNavData: function () {
    var navs = [];
    var nav0 = new Object();
    nav0.img = '../../images/nav/pyb.jpg';
    nav0.name = '拼音表';
    navs[0] = nav0;

    var nav1 = new Object();
    nav1.img = '../../images/nav/peom.png';
    nav1.name = '背古诗';
    navs[1] = nav1;

    var nav2 = new Object();
    nav2.img = '../../images/nav/cfb.png';
    nav2.name = '乘法表';
    navs[2] = nav2;

    var nav3 = new Object();
    nav3.img = '../../images/nav/jfb.png';
    nav3.name = '加法表';
    navs[3] = nav3;

    var nav4 = new Object();
    nav4.img = '../../images/nav/dwhs.png';
    nav4.name = '单位换算';
    navs[4] = nav4;

    var nav5 = new Object();
    nav5.img = '../../images/nav/rstx.png';
    nav5.name = '认识图形';
    navs[5] = nav5;

    var nav6 = new Object();
    nav6.img = '../../images/nav/letter_table.png';
    nav6.name = '字母表';
    navs[6] = nav6;

    var nav7 = new Object();
    nav7.img = '../../images/nav/word_search.png';
    nav7.name = '查单词';
    navs[7] = nav7;

    

    var nav9 = new Object();
    nav9.img = '../../images/nav/qbfl.jpg';
    nav9.name = '全部分类';
    navs[9] = nav9;
    return navs;
  }
})