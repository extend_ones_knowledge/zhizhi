// pages/life/life.js
//var common = require('../../utils/common.js') //引用公共JS文件
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //幻灯片素材
    
    lists:[]
  },

  onLoad: function (options) {
    var page = this;
    var lists = this.loadPoetrysListData();
    page.setData({ lists: lists });
  },
  listBtn: function (e) {
    console.log(e);
    var id = e.currentTarget.id;
    if (id == "0") {
      wx.navigateTo({
        url: '../life/lifes'
      })
    }
    
  },
  loadPoetrysListData: function () {
    var lists = [];
    var list0 = new Object();
    list0.id = '0';
    list0.title = '脚踝关节扭伤如何处理';
    list0.poster = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1570828384658&di=2f5765e8d8c1598cbcce67344c4010b7&imgtype=0&src=http%3A%2F%2Fwww.ymjr.net%2Fuploadfile%2F2014%2F1104%2F20141115.jpg';
    list0.add_date = '2019-10-10';
    lists[0] = list0;

    return lists;
  }
})