// pages/errquiz/errquiz.js
var app = getApp()
const db = wx.cloud.database()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    count:0,//数据的条数
    ret:[],//返回结果
    myopenid:'',

    navs: [],//题目数组
    unavs: [],
    uflag: [],
    navs1: "",//当前显示的题目的内容
    show_ans:[],

    inputText: ''

  },
  firstid: 0,
  nowid: 0,
  lastid: 0,
  makes: function () {//初始化题库
    //var page = this;
    //this.getCount()
    var that = this;

    var openid = this.data.myopenid

    var show=[]
    for(var j=0;j<this.data.count;j++){
        show[j]=''
    }
    that.setData({
      show_ans:show
    })
    wx.cloud.callFunction({
      name:'zhizhi_yun',

      success(res){
        console.log("删除成功")
      },
      fail:console.error
    })


    this.lastid = this.firstid + this.data.count 
    this.nowid = this.firstid

    console.log('数据数量是')
    console.log(this.data.count)

    var text = this.data.ret[this.nowid].CONT
    that.setData({
      navs1:text
    })
  },
  uans: '',//用户输入的答案
  ans0: function (e) {//获取用户答案
    this.uans = e.detail.value;
    var that = this
    var i = e.detail.value;
    this.data.ret[this.nowid].ANS = i//存储用户答案的数组

    if (i == this.data.ret[this.nowid].COR_ANS) {
      this.data.ret[this.nowid].COR = 1
    }
    else this.data.ret[this.nowid].COR = 0
  },
  answer: function () {//查看答案按钮
    /*
    if (!(/^[0-9]+$/.test(this.uans))){
      wx.showModal({
        title: '提示',

        content: '请输入数字',
        showCancel: false
      })

      return;
    }
    */
    var navs = this.data.ret//题目答案
    var unavs = this.data.unavs//用户答案
    var flag = this.data.uflag//正确标志
    var show = this.data.show_ans
    var strings

    for (var i = 0; i < this.data.count; i++) {//取出所有题目
      strings = '题目：' + navs[i].CONT + '你的答案：' + navs[i].ANS + '正确答案：' + show[i]
      wx.showModal({
        title: '提示',

        content: strings,
        showCancel: false
      })
    }
  },
  setquiz: function () {

  },
  next: function () {//下一道题
    this.nowid = this.nowid + 1
    var that = this

    if (this.nowid >= this.lastid) {
      this.nowid = this.nowid - 1
      wx.showModal({
        title: '提示',

        content: '已经是最后一题，请确认无误提交',
        showCancel: false
      })
      return
    }
    var input = this.data.ret[this.nowid].ANS
    that.setData({
      inputText: input
    })
    that.setData({
      navs1: ''
    })
    var text = this.data.ret[this.nowid].CONT
    that.setData({
      navs1: text
    })
  },
  back: function () {//上一道题
    this.nowid = this.nowid - 1
    var that = this

    if (this.nowid < this.firstid) {
      this.nowid = this.nowid + 1
      wx.showModal({
        title: '提示',

        content: '已经是第一题，不能往前了',
        showCancel: false
      })
      return
    }
    var input = this.data.ret[this.nowid].ANS
    that.setData({
      inputText: input
    })
    that.setData({
      navs1: ''
    })
    var text = this.data.ret[this.nowid].CONT
    that.setData({
      navs1: text
    })

  },
  submit: function () {
    var navs = this.data.ret
    var unavs = this.data.unavs
    var flag = this.data.uflag
    var show = this.data.show_ans
    var that = this

    for(var j=0;j<this.data.count;j++){
      show[j] = navs[j].COR_ANS
    }


    console.log('数据数量是')
    console.log(this.data.count)
    for (var i = 0; i < this.data.count; i++) {//取出所有题目
      db.collection('A-ku').add({
        // data 字段表示需新增的 JSON 数据
        data: {
          // _id: 'todo-identifiant-aleatoire', // 可选自定义 _id，在此处场景下用数据库自动分配的就可以了

          ANS: navs[i].ANS,
          CONT: navs[i].CONT,
          COR: navs[i].COR,
          COR_ANS: navs[i].COR_ANS,
          //UID: uid,
          Date: new Date(),
        },
        success: function (res) {
          // res 是一个对象，其中有 _id 字段标记刚创建的记录的 id
          console.log(res.data)
        }
      })
      console.log('插入成功')
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  
  onGetOpenid: function () {//获取用户信息
    // 调用云函数
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: res => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        app.globalData.openid = res.result.openid
        
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
        
      }
    })
  },
  setError:function(){
    
  },
  onLoad: function (options) {
    this.onGetOpenid()
    var that=this
    that.setData({
      myopenid:app.globalData.openid
    })

    var openid = this.data.myopenid
    var that = this
    db.collection('A-ku').where({
      COR: 0,
      _openid: openid
    })
      .get({
        success(res) {
          console.log(res.data)
          that.setData({
            ret: res.data
          })
        }
    })

    //this.getCount()

    var allitem = [];
    /*
    for(var i=0;i<this.data.count;i++){
      var item = new Object
      item.text = this.data.ret[i].CONT
      item.answer = this.data.ret[i].COR_ANS
      allitem.push(item)
      this.data.flag[i] = this.data.ret[i].COR
      this.data.unavs[i] = this.data.ret[i].ANS
      console.log("CONT COR_ANS COR ANS")
      console.log(item.text,item.answer,this.data.flag[i],this.data.unavs)
    }

    var that = this
    that.setData({
      navs : allitem
    })
    */
    this.getCount()
    console.log('数据数量是')
    console.log(this.data.count)

  },
  getCount:function(){
    
    var that = this
    var openid = this.data.myopenid
    db.collection('A-ku').where({
      COR: 0,
      _openid: openid
    }).count({
      success: function (res) {
        console.log(res.total)
        that.setData({
          count: res.total
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})